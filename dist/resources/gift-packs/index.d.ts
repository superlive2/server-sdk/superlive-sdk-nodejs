import { ResponseData } from '../../common/types';
import { Base } from '../base';
import { GiftPack, CreateGiftPackPayload, UpdateGiftPackPayload, FindAllGiftPackOptions } from './types';
export declare class GiftPacks extends Base {
    create(payload: CreateGiftPackPayload): Promise<ResponseData<GiftPack>>;
    update(id: string, payload: UpdateGiftPackPayload): Promise<ResponseData<string>>;
    getAll(options?: FindAllGiftPackOptions): Promise<ResponseData<GiftPack[]>>;
    getOne(id: string): Promise<ResponseData<GiftPack>>;
    deleteOne(id: string): Promise<ResponseData<string>>;
}
