import { ResponseData } from '../../common/types';
import { Base } from '../base';
import { CreateParticipantPayload, UpdateParticipantPayload, FindAllOptions, Participant } from './types';
export declare class Participants extends Base {
    create(payload: CreateParticipantPayload): Promise<ResponseData<Participant>>;
    update(id: string, payload: UpdateParticipantPayload): Promise<ResponseData<string>>;
    getAll(options?: FindAllOptions): Promise<ResponseData<Participant[]>>;
    getOne(id: string): Promise<ResponseData<Participant>>;
    deleteOne(id: string): Promise<ResponseData<string>>;
}
