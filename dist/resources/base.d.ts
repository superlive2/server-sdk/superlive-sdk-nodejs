import { AxiosInstance } from 'axios';
type Config = {
    apiKey: string;
    production?: boolean;
};
export declare abstract class Base {
    private apiKey;
    private apiEndpoint;
    protected httpReq: AxiosInstance;
    constructor(config: Config);
    private initAxios;
}
export {};
