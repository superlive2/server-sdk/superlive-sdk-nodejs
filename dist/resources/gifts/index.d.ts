import { ResponseData } from '../../common/types';
import { Base } from '../base';
import { CreateGiftPayload, UpdateGiftPayload, Gift, FindAllGiftOptions } from './types';
export declare class Gifts extends Base {
    create(payload: CreateGiftPayload): Promise<ResponseData<Gift>>;
    update(id: string, payload: UpdateGiftPayload): Promise<ResponseData<string>>;
    getAll(options?: FindAllGiftOptions): Promise<ResponseData<Gift[]>>;
    getOne(id: string): Promise<ResponseData<Gift>>;
    deleteOne(id: string): Promise<ResponseData<string>>;
}
