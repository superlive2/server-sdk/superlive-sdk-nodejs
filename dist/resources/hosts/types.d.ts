export declare type Host = {
    _id: string;
    name: string;
    username: string;
    password: string;
    description?: string;
    pushOptions: PushOption[];
    playOptions: PlayOption;
    thumbnailUrls: string[];
    giftPacks: string[];
    stickerPacks: string[];
    receivedGiftPoint: number;
    isChatEnabled: boolean;
    createdAt: string;
    updatedAt: string;
};
type PushOption = {
    type: string;
    url: string;
};
type PlayOption = {
    llhls: LlhlsPlayOption[];
    ws: WsPlayOption[];
};
type LlhlsPlayOption = {
    name: string;
    urls: string[];
};
type WsPlayOption = {
    name: string;
    urls: string[];
};
export declare type CreateHostPayload = {
    name: string;
    description?: string;
    username?: string;
    password?: string;
    isChatEnabled: boolean;
};
export declare type UpdateHostPayload = {
    name?: string;
    description?: string;
    username?: string;
    password?: string;
    isChatEnabled?: boolean;
};
export declare type FindAllOptions = {
    page?: number | string;
    limit?: number | string;
    search?: string;
};
export {};
