import { ResponseData } from '../../common/types';
import { Base } from '../base';
import { CreateHostPayload, UpdateHostPayload, FindAllOptions, Host } from './types';
export declare class Hosts extends Base {
    create(payload: CreateHostPayload): Promise<ResponseData<Host>>;
    update(id: string, payload: UpdateHostPayload): Promise<ResponseData<string>>;
    disable(id: string): Promise<ResponseData<string>>;
    enable(id: string): Promise<ResponseData<string>>;
    end(id: string): Promise<ResponseData<string>>;
    getAll(options?: FindAllOptions): Promise<ResponseData<Host[]>>;
    getOne(id: string): Promise<ResponseData<Host>>;
    deleteOne(id: string): Promise<ResponseData<string>>;
    count(): Promise<ResponseData<number>>;
}
