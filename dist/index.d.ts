import { GiftPacks } from './resources/gift-packs';
import { Gifts } from './resources/gifts';
import { Hosts } from './resources/hosts';
import { Participants } from './resources/participants';
export declare class SuperLiveSDK {
    hosts: Hosts;
    participants: Participants;
    giftPacks: GiftPacks;
    gifts: Gifts;
    constructor(config: {
        apiKey: string;
        production: boolean;
    });
}
