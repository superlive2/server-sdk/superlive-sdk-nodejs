export declare type Gift = {
    _id: string;
    name: string;
    createdAt: string;
    updatedAt: string;
}

export declare type CreateGiftPayload = {
    name: string;
    image: string;
    point: number;
    giftPack: string;
}

export declare type UpdateGiftPayload = {
    name?: string;
    image?: string;
    point?: number;
    giftPack?: string;
}

export declare type FindAllGiftOptions = {
    page?: number | string;
    limit?: number | string;
    search?: string;
}

