import { ResponseData } from '../../common/types';
import { Base } from '../base';
import { CreateGiftPayload, UpdateGiftPayload, Gift, FindAllGiftOptions } from './types';

export class Gifts extends Base {
    
    async create(payload: CreateGiftPayload): Promise<ResponseData<Gift>> {
        try {
            const response = await this.httpReq.post(`gifts`, payload);
            return response.data;
        } catch (error) {
            throw new Error(error.response?.data?.message || error.message);
        }
    }

    async update(id: string, payload: UpdateGiftPayload): Promise<ResponseData<string>> {
        try {
            const response = await this.httpReq.put(`gifts/${id}`, payload);
            return response.data;
        } catch (error) {
            throw new Error(error.response?.data?.message || error.message);
        }
    }

    async getAll(options?: FindAllGiftOptions): Promise<ResponseData<Gift[]>> {
        if (options?.limit) {
            options.limit = 100;
        }

        try {
            const response = await this.httpReq.get(`gifts`, { params: options });
            return response.data;
        } catch (error) {
            console.log('Err', error);
            throw new Error(error.response?.data?.message || error.message);
        }
    }

    async getOne(id: string): Promise<ResponseData<Gift>> {
        try {
            const response = await this.httpReq.get(`gifts/${id}`);
            return response.data;
        } catch (error) {
            throw new Error(error.response?.data?.message || error.message);
        }
    }

    async deleteOne(id: string): Promise<ResponseData<string>> {
        try {
            const response = await this.httpReq.delete(`gifts/${id}`);
            return response.data;
        } catch (error) {
            throw new Error(error.response?.data?.message || error.message);
        }
    }

}
