import { ResponseData } from '../../common/types';
import { Base } from '../base';
import { CreateParticipantPayload, UpdateParticipantPayload, FindAllOptions, Participant  } from './types';

export class Participants extends Base {

    async create(payload: CreateParticipantPayload): Promise<ResponseData<Participant>> {
        try {
            const response = await this.httpReq.post(`participants`, payload);
            return response.data;
        } catch (error) {
            throw new Error(error.response?.data?.message || error.message);
        }
    }

    async update(id: string, payload: UpdateParticipantPayload): Promise<ResponseData<string>> {
        try {
            const response = await this.httpReq.put(`participants/${id}`, payload);
            return response.data;
        } catch (error) {
            throw new Error(error.response?.data?.message || error.message);
        }
    }

    async getAll(options?: FindAllOptions): Promise<ResponseData<Participant[]>> {
        if (options?.limit) {
            options.limit = 100;
        }

        try {
            const response = await this.httpReq.get(`participants`, { params: options });
            return response.data;
        } catch (error) {
            console.log('Err', error);
            throw new Error(error.response?.data?.message || error.message);
        }
    }

    async getOne(id: string): Promise<ResponseData<Participant>> {
        try {
            const response = await this.httpReq.get(`participants/${id}`);
            return response.data;
        } catch (error) {
            throw new Error(error.response?.data?.message || error.message);
        }
    }

    async deleteOne(id: string): Promise<ResponseData<string>> {
        try {
            const response = await this.httpReq.delete(`participants/${id}`);
            return response.data;
        } catch (error) {
            throw new Error(error.response?.data?.message || error.message);
        }
    }

}
