export declare type Participant = {
    _id: string;
    name: string;
    description?: string;
    giftPoint: number;
    isHost: boolean;
    createdAt: string;
    updatedAt: string;
}

export declare type CreateParticipantPayload = {
    name: string;
    description?: string;
}

export declare type UpdateParticipantPayload = {
    name: string;
    description?: string;
}

export declare type FindAllOptions = {
    page?: number | string;
    limit?: number | string;
    search?: string;
}



