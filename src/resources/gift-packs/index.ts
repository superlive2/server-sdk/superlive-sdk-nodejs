import { ResponseData } from '../../common/types';
import { Base } from '../base';
import { GiftPack, CreateGiftPackPayload, UpdateGiftPackPayload, FindAllGiftPackOptions  } from './types';

export class GiftPacks extends Base {

    async create(payload: CreateGiftPackPayload): Promise<ResponseData<GiftPack>> {
        try {
            const response = await this.httpReq.post(`gifts/packs`, payload);
            return response.data;
        } catch (error) {
            throw new Error(error.response?.data?.message || error.message);
        }
    }

    async update(id: string, payload: UpdateGiftPackPayload): Promise<ResponseData<string>> {
        try {
            const response = await this.httpReq.put(`gifts/packs/${id}`, payload);
            return response.data;
        } catch (error) {
            throw new Error(error.response?.data?.message || error.message);
        }
    }

    async getAll(options?: FindAllGiftPackOptions): Promise<ResponseData<GiftPack[]>> {
        if (options?.limit) {
            options.limit = 100;
        }

        try {
            const response = await this.httpReq.get(`gifts/packs`, { params: options });
            return response.data;
        } catch (error) {
            console.log('Err', error);
            throw new Error(error.response?.data?.message || error.message);
        }
    }

    async getOne(id: string): Promise<ResponseData<GiftPack>> {
        try {
            const response = await this.httpReq.get(`gifts/packs/${id}`);
            return response.data;
        } catch (error) {
            throw new Error(error.response?.data?.message || error.message);
        }
    }

    async deleteOne(id: string): Promise<ResponseData<string>> {
        try {
            const response = await this.httpReq.delete(`gifts/packs/${id}`);
            return response.data;
        } catch (error) {
            throw new Error(error.response?.data?.message || error.message);
        }
    }
    
}
