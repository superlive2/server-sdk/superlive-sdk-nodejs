
export declare type GiftPack = {
    _id: string;
    name: string;
    createdAt: string;
    updatedAt: string;
}

export declare type CreateGiftPackPayload = {
    name: string;
}

export declare type UpdateGiftPackPayload = {
    name: string;
}

export declare type FindAllGiftPackOptions = {
    page?: number | string;
    limit?: number | string;
    search?: string;
}


