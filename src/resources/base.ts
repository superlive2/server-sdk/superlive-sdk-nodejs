import axios, { AxiosInstance } from 'axios';

type Config = {
    apiKey: string;
    production?: boolean;
};

export abstract class Base {
    private apiKey: string;
    private apiEndpoint: string;
    protected httpReq: AxiosInstance;

    constructor(config: Config) {
        this.apiKey = config.apiKey;
        this.apiEndpoint = config.production
            ? 'https://merchant.super-live.tv/api/server-sdk'
            : 'http://merch.sp.tv/api/server-sdk';
        this.initAxios();
    }

    private initAxios() {
        this.httpReq = axios.create({
            baseURL: this.apiEndpoint,
            headers: { Authorization: this.apiKey }
        });
    }
}
