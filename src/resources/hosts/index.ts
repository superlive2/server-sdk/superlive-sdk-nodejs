import { ResponseData } from '../../common/types';
import { Base } from '../base';
import { CreateHostPayload, UpdateHostPayload, FindAllOptions, Host  } from './types';

export class Hosts extends Base {

    async create(payload: CreateHostPayload): Promise<ResponseData<Host>> {
        try {
            const response = await this.httpReq.post(`hosts`, payload);
            return response.data;
        } catch (error) {
            throw new Error(error.response?.data?.message || error.message);
        }
    }

    async update(id: string, payload: UpdateHostPayload): Promise<ResponseData<string>> {
        try {
            const response = await this.httpReq.put(`hosts/${id}`, payload);
            return response.data;
        } catch (error) {
            throw new Error(error.response?.data?.message || error.message);
        }
    }

    async disable(id: string): Promise<ResponseData<string>> {
        try {
            const response = await this.httpReq.post(`hosts/${id}/disable`);
            return response.data;
        } catch (error) {
            throw new Error(error.response?.data?.message || error.message);
        }
    }

    async enable(id: string): Promise<ResponseData<string>> {
        try {
            const response = await this.httpReq.post(`hosts/${id}/enable`);
            return response.data;
        } catch (error) {
            throw new Error(error.response?.data?.message || error.message);
        }
    }

    async end(id: string): Promise<ResponseData<string>> {
        try {
            const response = await this.httpReq.post(`hosts/${id}/end`);
            return response.data;
        } catch (error) {
            throw new Error(error.response?.data?.message || error.message);
        }
    }

    async getAll(options?: FindAllOptions): Promise<ResponseData<Host[]>> {
        if (options?.limit) {
            options.limit = 100;
        }

        try {
            const response = await this.httpReq.get(`hosts`, { params: options });
            return response.data;
        } catch (error) {
            console.log('Err', error);
            throw new Error(error.response?.data?.message || error.message);
        }
    }

    async getOne(id: string): Promise<ResponseData<Host>> {
        try {
            const response = await this.httpReq.get(`hosts/${id}`);
            return response.data;
        } catch (error) {
            throw new Error(error.response?.data?.message || error.message);
        }
    }

    async deleteOne(id: string): Promise<ResponseData<string>> {
        try {
            const response = await this.httpReq.delete(`hosts/${id}`);
            return response.data;
        } catch (error) {
            throw new Error(error.response?.data?.message || error.message);
        }
    }

    async count(): Promise<ResponseData<number>> {
        try {
            const response = await this.httpReq.get(`hosts/count`);
            return response.data;
        } catch (error) {
            throw new Error(error.response?.data?.message || error.message);
        }
    }
}
