
export declare type ResponseData<T> = {
    satusCode: number;
    data: T;
    extra: any;
}