import { GiftPacks } from './resources/gift-packs';
import { Gifts } from './resources/gifts';
import { Hosts } from './resources/hosts';
import { Participants } from './resources/participants';

export class SuperLiveSDK {
  hosts: Hosts;
  participants: Participants;
  giftPacks: GiftPacks;
  gifts: Gifts;

  constructor(config: { apiKey: string, production: boolean }) {
    this.hosts = new Hosts(config);
    this.participants = new Participants(config);
    this.giftPacks = new GiftPacks(config);
    this.gifts = new Gifts(config);
  }

}